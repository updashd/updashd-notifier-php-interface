<?php
namespace Updashd\Notifier;

use Updashd\Model\Incident;

class NoticeData {
    protected $incident;

    /**
     * @return Incident
     */
    public function getIncident () {
        return $this->incident;
    }

    /**
     * @param Incident $incident
     */
    public function setIncident ($incident) {
        $this->incident = $incident;
    }
}