<?php
namespace Updashd\Notifier;

use Updashd\Configlib\Config;

interface NotifierInterface {

    /**
     * Get the readable name of the notice service
     */
    public static function getReadableName ();

    /**
     * Get the name of the service (this should match in the database)
     * @return string
     */
    public static function getNotifierName ();

    /**
     * Create and return a Config object for this service
     * @return Config
     */
    public static function createConfig ();

    /**
     * Create a worker instance for the given service
     * @param Config $config the Config object used for configuration
     */
    public function __construct (Config $config);

    /**
     * Run the given test
     * @param NoticeData $noticeData
     * @return bool true if successful, false otherwise.
     */
    public function send (NoticeData $noticeData);
}